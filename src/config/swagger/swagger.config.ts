import { SwaggerConfig } from './swagger.interface';

/**
 * swagger config object
 */
export const SWAGGER_CONFIG: SwaggerConfig = {
  /**
   * title property
   */
  title: 'HOTEL-MANGEMENT',
  /**
   * description property
   */
  description: 'Hotel management',
  /**
   * version property
   */
  version: '1.0',
  /**
   * tags property
   */
  tags: ['templete'],
};
