import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { HotelType } from './hotelType.entity';
import { Maintainance } from './maintainance.entity';
import { RoomInfo } from './room.entity';

@Entity()
export class Hotel extends Maintainance {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  HotelName: string;

  @ApiProperty()
  @Column()
  city: string;

  @ApiProperty()
  @Column()
  state: string;

  @ApiProperty()
  @Column()
  location: string;

  @ApiProperty()
  @Column()
  status: string;

  @ApiProperty()
  @Column()
  noOfRooms: string;

  @ApiProperty()
  @Column()
  description: string;

  @ApiProperty()
  @Column()
  swimmingPool: boolean;
  @ApiProperty()
  @Column()
  gym: boolean;

  @ApiProperty()
  @Column()
  lakeView: boolean;

  @ApiProperty()
  @Column()
  beachView: boolean;

  @ApiProperty()
  @Column()
  indoorGames: boolean;

  @ApiProperty()
  @Column()
  outDoorGames: boolean;

  @ApiProperty()
  @Column()
  buffet: boolean;

  @ManyToOne(() => HotelType, (hotelType) => hotelType.hotel, {
    cascade: true,
  })
  @JoinColumn()
  hotelType: HotelType;

  @OneToMany(() => RoomInfo, (room) => room.hotel, {
    cascade: true,
  })
  @JoinColumn()
  room: RoomInfo;
}
