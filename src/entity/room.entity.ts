import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Hotel } from './hotel.entity';
import { RoomCategory } from './roomCategory.entity';

@Entity()
export class RoomInfo {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  TV: boolean;

  @ApiProperty()
  @Column()
  AC: boolean;

  @ApiProperty()
  @Column()
  balcony: boolean;

  @ApiProperty()
  @Column()
  Wifi: boolean;

  @ApiProperty()
  @Column()
  attachedBathroom: boolean;

  @ApiProperty()
  @Column()
  powerBackup: boolean;

  @ApiProperty()
  @Column()
  elevator: number;

  @ApiProperty()
  @Column()
  CCTVCameras: boolean;

  @ApiProperty()
  @Column()
  dailyHouseKeeping: boolean;

  @ApiProperty()
  @Column()
  DTH: boolean;

  @ApiProperty()
  @Column()
  bathTub: boolean;

  @ManyToOne(() => RoomCategory, (roomCategory) => roomCategory.room, {
    cascade: true,
  })
  roomCategory: RoomCategory;

  @ManyToOne(() => Hotel, (hotel) => hotel.room, {})
  hotel: Hotel;
}
