import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Hotel } from './hotel.entity';

@Entity()
export class HotelType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  hotelType: string;

  @OneToMany(() => Hotel, (hotel) => hotel.hotelType)
  hotel: Hotel[];

  addHotel(hotel: Hotel) {
    if (this.hotel == null) {
      this.hotel = new Array<Hotel>();
    }
    return this.hotel.push(hotel);
  }
}
