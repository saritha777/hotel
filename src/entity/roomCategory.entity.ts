import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { RoomInfo } from './room.entity';

@Entity()
export class RoomCategory {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  roomCategory: string;

  @OneToMany(() => RoomInfo, (roomInfo) => roomInfo.roomCategory)
  room: RoomInfo[];

  addRoom(room: RoomInfo) {
    if (this.room == null) {
      this.room = new Array<RoomInfo>();
    }
    return this.room.push(room);
  }
}
