import { ApiProperty } from '@nestjs/swagger';
import {
  IsBoolean,
  IsInt,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserLogin } from './login.entity';
import { Maintainance } from './maintainance.entity';

/**
 * user entity
 */
@Entity()
export class User extends Maintainance {
  /**
   * id as primary generated column
   */
  @PrimaryGeneratedColumn()
  id: number;

  /**
   * name property
   */
  @ApiProperty()
  @IsString()
  @Column()
  name: string;

  /**
   * password property
   */
  @ApiProperty()
  @IsString()
  @MinLength(4)
  @MaxLength(10)
  @Column()
  password: string;

  @ApiProperty()
  @IsString()
  @Column()
  email: string;

  @ApiProperty()
  @IsString()
  @Column()
  phoneNo: string;

  @ApiProperty()
  @IsString()
  @Column()
  role: string;

  @OneToOne(() => UserLogin, (login) => login.user, {
    cascade: true,
  })
  login: UserLogin;
}
