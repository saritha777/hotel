import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { globalAccess } from '../constants/globalAccess';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    //  const user = new User();
    const roles = this.reflector.get<string>('roles', context.getHandler());

    console.log(roles);

    if (!roles) {
      return true;
    }

    console.log('role:', globalAccess.role);

    if (roles.includes('admin') && globalAccess.role == 'admin') {
      return true;
    } else if (
      roles.includes('receptionist') &&
      globalAccess.role == 'receptionist'
    ) {
      return true;
    }
    return false;
  }
}
