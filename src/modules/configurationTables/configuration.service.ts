import { CacheKey, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { HotelType } from 'src/entity/hotelType.entity';
import { RoomCategory } from 'src/entity/roomCategory.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ConfigurationService {
  constructor(
    @InjectRepository(HotelType)
    private hotelTypeRepository: Repository<HotelType>,

    @InjectRepository(RoomCategory)
    private roomCategoryRepository: Repository<RoomCategory>,
  ) {}

  @CacheKey('hotel')
  getAllHotelTypes() {
    //return this.hotelTypeRepository.find()
    return new Promise((res) => {
      setTimeout(() => {
        res(this.hotelTypeRepository.find());
      }, 3000);
    });
  }

  getAllRoomCategories() {
    return this.roomCategoryRepository.find();
  }
}
