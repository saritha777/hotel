import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HotelType } from 'src/entity/hotelType.entity';
import { RoomCategory } from 'src/entity/roomCategory.entity';
import { ConfigurationService } from './configuration.service';

@Module({
  imports: [TypeOrmModule.forFeature([HotelType, RoomCategory])],
  providers: [ConfigurationService],
})
export class ConfigurationModule {}
