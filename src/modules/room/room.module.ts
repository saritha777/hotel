import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoomInfo } from 'src/entity/room.entity';
import { RoomCategory } from 'src/entity/roomCategory.entity';
import { RoomController } from './room.controller';
import { RoomService } from './room.service';

@Module({
  imports: [TypeOrmModule.forFeature([RoomCategory, RoomInfo])],
  controllers: [RoomController],
  providers: [RoomService],
})
export class RoomModule {}
