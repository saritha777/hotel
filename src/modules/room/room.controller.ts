import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Put,
  Delete,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { RoomInfo } from 'src/entity/room.entity';
import { RoomService } from './room.service';

@ApiTags('ROOMDATA')
@Controller('/roomdata')
export class RoomController {
  /**
   * injecting room service
   * @param roomService injecting room service
   */
  constructor(private readonly roomService: RoomService) {}

  /**
   * add method for room details
   * @param categoryId taking category form room category
   * @param data taking room data
   * @returns save the data into room info
   */
  @Post('/:categoryId')
  async addroom(
    @Param('categoryId') categoryId: number,
    @Body() room: RoomInfo,
  ) {
    return await this.roomService.addRoom(categoryId, room);
  }

  /**
   * getting all rooms
   * @returns all rooms details
   */
  @Get()
  async getAllRoomsData() {
    return await this.roomService.getAllRoom();
  }

  /**
   * update method for room
   * @param id taking id which id we want to update
   * @param categoryId taking this from category info
   * @param data room data
   * @returns updated info
   */
  @Put('/:id/:categoryId')
  async updateRoom(
    @Param('id') id: number,
    @Param('categoryId') categoryId: number,
    @Body() data: RoomInfo,
  ) {
    return this.roomService.updateRoom(id, categoryId, data);
  }

  /**
   * delete method for room
   * @param id taking id for delete
   * @returns deleted info
   */
  @Delete('/:id')
  async deleteRoom(@Param('id') id: number) {
    return this.deleteRoom(id);
  }
}
