import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RoomInfo } from 'src/entity/room.entity';
import { RoomCategory } from 'src/entity/roomCategory.entity';
import { Repository } from 'typeorm';

/**
 * room service
 */
@Injectable()
export class RoomService {
  /**
   *injecting repositories
   * @param roomRepository injecting room repository for storing and fetching data in room info
   * @param roomCategoryRepository injecting room type repository for storing and fetching data in room type info
   */
  constructor(
    @InjectRepository(RoomInfo)
    private roomRepository: Repository<RoomInfo>,
    @InjectRepository(RoomCategory)
    private roomCategoryRepository: Repository<RoomCategory>,
  ) {}

  /**
   * add method for room details
   * @param categoryId taking category form room category
   * @param data taking room data
   * @returns save the data into room info
   */
  async addRoom(categoryId: number, data: RoomInfo): Promise<RoomInfo> {
    const roomData = new RoomInfo();
    const roomCategory = await this.roomCategoryRepository.findOne({
      id: categoryId,
    });
    roomData.roomCategory = roomCategory;
    const newData = Object.assign(roomData, data);
    return this.roomRepository.save(newData);
  }

  /**
   * getting all rooms
   * @returns all rooms details
   */
  async getAllRoom(): Promise<RoomInfo[]> {
    return this.roomRepository.find();
  }

  /**
   * update method for room
   * @param id taking id which id we want to update
   * @param categoryId taking this from category info
   * @param data room data
   * @returns updated info
   */
  async updateRoom(id: number, categoryId: number, data: RoomInfo) {
    const roomData = new RoomInfo();
    const roomCategory = await this.roomCategoryRepository.findOne({
      id: categoryId,
    });
    roomData.roomCategory = roomCategory;
    const newData = Object.assign(roomData, data);
    await this.roomRepository.update({ id: id }, newData);
    return 'updated succefully';
  }

  /**
   * delete method for room
   * @param id taking id for delete
   * @returns deleted info
   */
  async deleteroom(id: number) {
    return this.roomRepository.delete({ id: id });
  }
}
