import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  Req,
  Res,
} from '@nestjs/common';
import { UserLogin } from 'src/entity/login.entity';
import { User } from 'src/entity/user.entity';
import { UserService } from './user.service';
import { Request, Response } from 'express';
import { ApiTags } from '@nestjs/swagger';

/**
 * controller that can receive inbound requests and produce responses
 */
@ApiTags('user')
@Controller('/user')
export class UserController {
  /**
   *injecting user service
   * @param userService
   */
  constructor(private readonly userService: UserService) {}
  /**
   * write adduser method to get user details
   * @param user user entity taken as parameter
   * @returns added user details
   */
  @Post()
  async addUser(@Body() user: User): Promise<User> {
    return await this.userService.addUser(user);
  }

  /**
   * post method for login
   * @param login taking login data
   * @param response taking response
   * @param request getting request obj
   * @returns string the user is loged in or not
   */
  @Post('/login')
  async login(
    @Body() login: UserLogin,
    @Res({ passthrough: true }) response: Response,
    @Req() request: Request,
  ) {
    return await this.userService.login(login, response, request);
  }

  /**
   * get method for all users
   * @returns all users
   */
  @Get()
  async getAll() {
    return this.userService
      .getAll()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException('user not found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new HttpException('user not found', HttpStatus.NOT_FOUND);
      });
  }
}
