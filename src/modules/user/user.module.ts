import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Hotel } from 'src/entity/hotel.entity';
import { HotelType } from 'src/entity/hotelType.entity';
import { RoomInfo } from 'src/entity/room.entity';
import { RoomCategory } from 'src/entity/roomCategory.entity';
import { JwtToken } from '../../common/providers/jwtservice';
import { UserLogin } from '../../entity/login.entity';
import { User } from '../../entity/user.entity';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      UserLogin,
      HotelType,
      RoomCategory,
      Hotel,
      RoomInfo,
    ]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '30m' },
    }),
  ],
  controllers: [UserController],
  providers: [UserService, JwtToken],
})
export class UserModule {}
