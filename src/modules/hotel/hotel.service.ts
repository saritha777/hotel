import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Hotel } from 'src/entity/hotel.entity';
import { HotelType } from 'src/entity/hotelType.entity';
import { Repository } from 'typeorm';

@Injectable()
export class HotelService {
  constructor(
    @InjectRepository(Hotel)
    private hotelRepository: Repository<Hotel>,
    @InjectRepository(HotelType)
    private hotelTypeRepository: Repository<HotelType>,
  ) {}

  async addHotel(hotelTypeId: number, data: Hotel) {
    const hotelData = new Hotel();
    const hotelType = await this.hotelTypeRepository.findOne({
      id: hotelTypeId,
    });
    hotelData.hotelType = hotelType;
    const newData = Object.assign(hotelData, data);
    return this.hotelRepository.save(newData);
  }
}
